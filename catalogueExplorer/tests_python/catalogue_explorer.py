import sys
from checker import check_source, file_exc
from search import search_tool

def show_usage():
    print("Usage:\npython3 catalogue_explorer.py <chemin du fichier>", flush=True)

def start():
    catalogue_path = _check_filepath()
    catalogue_searcher = search_tool(catalogue_path)
    catalogue_searcher.preview_catalogue()
    search_again = True
    while search_again :
        search_lookup = input("Entrez votre recherche (titre, éditeur, thématique) : ")
        catalogue_searcher.search(search_lookup)
        user_choice = input("Continuer ? (o/n) ")
        search_again = user_choice.lower() == 'o'


def _check_filepath():
    has_filepath_arg = sys.argv.__len__() == 2
    if not has_filepath_arg :
        show_usage()
        exit()
    try :
        check_source(sys.argv[1])
        return sys.argv[1]
    except file_exc as fe:
        print(fe)
        exit()

if __name__ == "__main__" :
    start()
