from os import access, R_OK
from os.path import isfile

class file_exc(Exception) :
    def __str__(self):
        raise NotImplementedError

class filetype_exc(file_exc):
    def __str__(self) :
        return "Wrong file type: only .txt and .tsv files accepted"

class fileaccess_exc(file_exc):
    def __init__(self, filepath) :
        self._filepath = filepath

    def __str__(self) :
        return f"{self._filepath} cannot be read or does not exist"

def check_source(file_path):
    valid_extensions = ['tsv', 'txt']
    try:
        if not (isfile(file_path) and access(file_path, R_OK)):
            raise fileaccess_exc(file_path)
        file_parts = file_path.split('.')
        extension_ok = file_parts[-1] in valid_extensions
        assert(extension_ok)
    except AssertionError as e:
        raise filetype_exc from e
    except IndexError as ie:
        raise filetype_exc from ie