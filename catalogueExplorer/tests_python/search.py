import csv
from book import book

class search_tool:

    _delimiter = '\t'
    _barcode_key = "CODE ISBN"
    _title_key = "  TITRE LONG"
    _publisher_key = "  NOM EDITEUR"
    _topic_key = "Thématique"
    _preview_len = 5

    def __init__(self, file_path):
        self._file_path = file_path

    def preview_catalogue(self):
        with open(self._file_path) as tsv_file:
            tsv_reader = csv.DictReader(tsv_file, delimiter=self._delimiter)
            books = [book(row[self._title_key], row[self._publisher_key], row[self._topic_key], row[self._barcode_key]) for row in tsv_reader]
            print("Aperçu du catalogue\n", flush=True)
            for item in books[:self._preview_len]:
                print(item, flush=True)
                print("***", flush=True)
            print("", flush=True)

    def search(self, lookup_pattern):
        # TODO : replace by implementation
        print("TODO : implement search method", flush=True)
