class book:

    def __init__(self, title, publisher, topic, barcode) :
        self.title = title
        self.publisher = publisher
        self.topic = topic
        self.barcode = barcode

    def __str__(self) :
        return f"{self.title}\nÉditeur: {self.publisher}\nThématique: {self.topic}\nEAN: {self.barcode}"
