import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVRecord
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.nio.charset.Charset
import java.nio.file.Paths

class SearchTool(private val catalog: String) {
    private val delimiter = '\t'
    private val barcodeKey = "CODE ISBN"
    private val titleKey = "  TITRE LONG"
    private val publisherKey = "  NOM EDITEUR"
    private val topicKey = "Thématique"
    private val previewLen = 5

    fun previewCatalogue() {
        val parser = getCsvParser()

        println("\nAperçu du catalogue\nFichier source: $catalog\n***\n")
        for (record in parser.records.take(previewLen)) {
            val book = getBookFromRecord(record)
            println(book)
        }
        println("***\n")
    }

    fun search(pattern: String) {
        // TODO replace the code below by implementation
        println("Pending implementation for searching $pattern in $catalog")
    }

    private fun getCsvParser() : CSVParser {
        val bufferedReader = File(catalog).bufferedReader(Charset.forName("windows-1252"))

        val format = CSVFormat.DEFAULT
            .withDelimiter(delimiter)
            .withFirstRecordAsHeader()

        return  CSVParser(bufferedReader,format)
    }

    private fun getBookFromRecord(record: CSVRecord) : Book {
        val title = record.get(titleKey)
        val publisher = record.get(publisherKey)
        val barcode = record.get(barcodeKey)
        val topic = record.get(topicKey)

        return Book(title, publisher, topic, barcode)
    }
}