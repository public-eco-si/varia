data class Book (
    val title: String,
    val publisher: String,
    val topic: String,
    val barcode: String
)