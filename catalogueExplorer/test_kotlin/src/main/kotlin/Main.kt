fun main(args: Array<String>) {
    println("Program arguments: ${args.joinToString()}")

    val cataloguePath = checkFilePath(args)
    if (cataloguePath.isNullOrEmpty()){
        return
    }
    try {
        val searchTool = SearchTool(cataloguePath)
        searchTool.previewCatalogue()

        do {
            val lookup = queryUserSearch()
            searchTool.search(lookup)
        } while (userContinues())
    }
    catch (e: Exception) {
        println(e.message)
    }
}

fun checkFilePath(args: Array<String>) : String{
    if (args.size != 1) {
        showUsage()
        return ""
    }
    return args[0]
}

fun showUsage() {
    val message = "Usage:\njava -jar test_kotlin-1.0-SNAPSHOT.jar <catalogue path>"
    println(message)
}

fun userContinues() : Boolean {
    println("Continuer? (o/n)")
    val userChoice = readLine()
    val choices = arrayOf('o', 'O')
    return choices.contains(userChoice?.first())
}

fun queryUserSearch() : String {
    println("Entrez votre recherche (titre, éditeur, thématique)")
    val lookup = readLine()
    return if (lookup.isNullOrEmpty()) "" else lookup
}