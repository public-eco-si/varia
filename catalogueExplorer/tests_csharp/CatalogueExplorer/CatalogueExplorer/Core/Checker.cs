﻿using CatalogueExplorer.Exceptions;
using System;
using System.IO;
using System.Linq;

namespace CatalogueExplorer.Core
{
    static class Checker
    {
        public static void CheckSource(string filePath)
        {
            var validExtensions = new[] { "tsv", "txt" };
            var fileInfo = new FileInfo(filePath);

            if (!fileInfo.Exists)
            {
                throw new FileAccessException($"{filePath} does not exist or is not a file");
            }
            try
            {
                var fileStream = File.OpenRead(filePath);
                var nameParts = filePath.Split('.');
                var fileExtension = nameParts.Last();
                if (!validExtensions.Contains(fileExtension))
                {
                    throw new FileTypeException($"Wrong file type: only .txt and .tsv files accepted");
                }
            }
            catch (Exception e)
            {
                throw new FileAccessException($"{filePath} cannot be read by current user", e);
            }
        }
    }
}
