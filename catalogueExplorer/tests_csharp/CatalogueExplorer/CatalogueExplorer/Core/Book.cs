﻿using CsvHelper.Configuration.Attributes;

namespace CatalogueExplorer.Core
{
    public class Book
    {
        public Book()
        { }

        public Book(string title, string publisher, string topic, string barcode)
        {
            Title = title;
            Publisher = publisher;
            Topic = topic;
            Barcode = barcode;
        }

        public string Title { get; set; }

        public string Publisher { get; set; }

        public string Topic { get; set; }

        public string Barcode { get; set; }

        public override string ToString()
        {
            return $"{Title}\nÉditeur: {Publisher}\nThématique: {Topic}\nEAN: {Barcode}";
        }
    }
}
