﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace CatalogueExplorer.Core
{
    class SearchTool
    {
        private string filePath;
        private string delimiter = "\t";
        private int previewLen = 5;
        private CsvConfiguration csvConfig;
        private Encoding encoding;

        public SearchTool(string filePath)
        {
            this.filePath = filePath;
            encoding = CodePagesEncodingProvider.Instance.GetEncoding(1252);
            csvConfig = new CsvConfiguration(CultureInfo.InvariantCulture, delimiter: delimiter);
        }

        public void PreviewCatalogue()
        {
            using var streamReader = new StreamReader(filePath, encoding);
            using var csvReader = new CsvReader(streamReader, csvConfig);

            csvReader.Context.RegisterClassMap<BookMap>();
            var records = csvReader.GetRecords<Book>().Take(previewLen);

            Console.WriteLine("Aperçu du catalogue\n");
            foreach (var item in records)
            {
                Console.WriteLine(item);
                Console.WriteLine("***");
            }
            Console.WriteLine();
        }

        public void Search(string lookup_pattern)
        {
            // TODO : replace by implementation
            Console.WriteLine("TODO : implement search method");
        }
    }
}
