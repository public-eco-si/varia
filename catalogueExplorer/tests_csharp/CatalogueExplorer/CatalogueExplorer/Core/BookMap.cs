﻿using CsvHelper.Configuration;

namespace CatalogueExplorer.Core
{
    class BookMap : ClassMap<Book>
    {
        private const string barcodeKey = "CODE ISBN";
        private const string titleKey = "  TITRE LONG";
        private const string publisherKey = "  NOM EDITEUR";
        private const string topicKey = "Thématique";

        public BookMap()
        {
            Map(b => b.Barcode).Name(barcodeKey);
            Map(b => b.Title).Name(titleKey);
            Map(b => b.Publisher).Name(publisherKey);
            Map(b => b.Topic).Name(topicKey);
        }
    }
}
