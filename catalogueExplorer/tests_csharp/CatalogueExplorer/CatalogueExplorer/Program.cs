﻿using CatalogueExplorer.Core;
using System;
using System.Linq;

namespace CatalogueExplorer
{
    class Program
    {
        static void Main(string[] args)
        {
            CheckFilePath(args, out var cataloguePath);
            var catalogueSearcher = new SearchTool(cataloguePath);
            catalogueSearcher.PreviewCatalogue();

            do
            {
                var searchLookup = QueryUserSearch();
                catalogueSearcher.Search(searchLookup);
            }
            while (UserContinues());
        }

        private static string QueryUserSearch()
        {
            Console.WriteLine("Entrez votre recherche (titre, éditeur, thématique) :");
            var lookup_pattern = Console.ReadLine();

            return lookup_pattern;
        }

        private static bool UserContinues()
        {
            Console.WriteLine("Continuer? (o/n)");
            var userChoice = Console.ReadKey().KeyChar;
            var continueExecution = new[] { 'o', 'O' }.Contains(userChoice);
            Console.WriteLine();

            return continueExecution;
        }

        static void CheckFilePath(string[] args, out string filePath)
        {
            var hasFilePathArg = args.Length == 1;
            filePath = string.Empty;
            if (!hasFilePathArg)
            {
                ShowUsage();
                Environment.Exit(1);
            }

            try
            {
                filePath = args[0];
                Checker.CheckSource(filePath);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Environment.Exit(1);
            }
        }

        static void ShowUsage()
        {
            Console.WriteLine("Usage:\nCatalogueExplorer.exe <chemin du fichier>");
        }
    }
}
