﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CatalogueExplorer.Exceptions
{
    public class FileAccessException : Exception
    {
        public FileAccessException() : base()
        { }

        public FileAccessException(string message) : base(message)
        { }

        public FileAccessException(string message, Exception inner) : base(message, inner)
        { }
    }
}
